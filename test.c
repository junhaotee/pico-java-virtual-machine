#include <stdio.h>

short* stackPtr,localVarPtr,heapPtr;

typedef void (*funcPtr)(short* stackPtr,short* localVarPtr,short* heapPtr);

typedef struct{

	short opcode_size;
	funcPtr interpreter_ptr;
	char* name;

}bytecode_struct;

void bipush(short* stackPtr, short* localVarPtr, short* heapPtr){
	printf("stack:%x\nlocalVar%x\nheap:%x\n",stackPtr,localVarPtr,heapPtr);
}

int main(){

	void (*interpreter_bipush)(short*,short*,short*);
	interpreter_bipush=bipush;
	bytecode_struct bytecode[]={{2,bipush,"asdf"}};
	printf("%d\n",bytecode[0].opcode_size);
	
	short k=3;
	short* j;
	j=&k;
	bytecode[0].interpreter_ptr(j,j,NULL);
	
	

}
