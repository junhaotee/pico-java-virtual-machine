#define ACC_PUBLIC	0x0001
#define ACC_PRIVATE	0X0002
#define ACC_PROTECTED	0x0004
#define ACC_STATIC	0x0008
#define ACC_FINAL 	0x0010
#define ACC_SUPER	0x0020
#define ACC_VOLATILE	0x0040
#define ACC_TRANSIENT	0x0080
#define ACC_INTERFACE	0x0200
#define ACC_ABSTRACT	0x0400
#define ACC_SYNTHETIC	0x1000
#define ACC_ANNOTATION	0x2000
#define ACC_ENUM	0x4000

#define CONSTANT_Class 7
#define CONSTANT_Fieldref 9
#define CONSTANT_Methodref 10
#define CONSTANT_InterfaceMethodref 11
#define CONSTANT_String 8
#define CONSTANT_Integer 3
#define CONSTANT_Float 4
#define CONSTANT_Long 5
#define CONSTANT_Double 6
#define CONSTANT_NameAndType 12
#define CONSTANT_Utf8 1
#define CONSTANT_MethodHandle 15
#define CONSTANT_MethodType 16
#define CONSTANT_InvokeDynamic 18

typedef struct{
	unsigned short	attribute_name_index,
			constantvalue_index;
	unsigned int	attribute_length;
}ConstantValue_attribute;


typedef struct{
	unsigned short	index,
			class_index,
			name_and_type_index,
			string_index,
			name_index,
			descriptor_index,
			length,
			reference_index,
			bootstrap_method_attr_index;

	unsigned int	bytes,
			high_bytes,
			low_bytes;
					
	char		*base,
			tag,
			reference_kind;

}constant_info_struct;

typedef struct{
	char tag;
	unsigned short	name_index;	
}CONSTANT_Class_info;

typedef struct{
	char tag;
	unsigned short	class_index,
			name_and_type_index;
}CONSTANT_Fieldref_info;

typedef struct{
	char tag;
	unsigned short	class_index,
			name_and_type_index;
}CONSTANT_Methodref_info;

typedef struct{
	char tag;
	unsigned short	class_index,
			name_and_type_index;
	
}CONSTANT_InterfaceMethodref_info;


typedef struct{
	char tag;
	unsigned short	string_index;
}CONSTANT_String_info;


typedef struct{
	char tag;
	unsigned int bytes;
}CONSTANT_Integer_info;


typedef struct{
	char tag;
	unsigned int bytes;
}CONSTANT_Float_info;


typedef struct{
	char tag;
	unsigned int	high_bytes,
			low_bytes;
}CONSTANT_Long_info;


typedef struct{
	char tag;
	unsigned int	high_bytes,
			low_bytes;	
}CONSTANT_Double_info;


typedef struct{
	char tag;
	unsigned short	name_index,
			descriptor_index;
}CONSTANT_NumAndType_info;


typedef struct{
	char tag;
	unsigned short	length;
	char	bytes[];
}CONSTANT_Utf8_info;


typedef struct{
	char	tag,
		reference_kind;
	unsigned short	reference_index;
}CONSTANT_MethodHandle_info;


typedef struct{
	char tag;
	unsigned short	descriptor_index;
}CONSTANT_MethodType_info;


typedef struct{
	char tag;
	unsigned short	bootstrap_metho_attr_index,
			name_and_type_index;
}CONSTANT_InvokeDynamic_info;



typedef struct{
	unsigned short 	attribute_name_index;
	unsigned int 	attribute_length;
	char 		info[];
}attribute_info;


typedef struct{
	unsigned short 	access_flags,
			name_index,
			descriptor_index,
			attributes_count;
	ConstantValue_attribute	ConstantValue_attribute;
}field_info;

typedef struct{
	unsigned short	attribute_name_index,
			number_of_entries;
	unsigned int	attribute_length;
	/*stack map frame*/

}stack_map_table;

typedef struct{
	unsigned short	access_flags,
			name_index,
			descriptor_index,
			attributes_count;
}method_info;

typedef struct{

}stack_map_frame;

typedef struct{
	unsigned short	attribute_name_index,
			number_of_entries;
	unsigned int	attribute_length;
	stack_map_frame	stack_map_frame;

}StackMapTable_attribute;

typedef struct{
	unsigned short	start_pc,line_number;
}line_number_table;

typedef struct{
	unsigned short	attribute_name_index,
			line_number_table_length;
	unsigned int	attribute_length;
	line_number_table	line_number_table[];
}LineNumberTable_attribute;

typedef struct{
	unsigned short	attribute_name_index,
			max_stack,
			max_locals,
			exception_table_length,
			attributes_count;
	unsigned int	attribute_length,
			code_length;
	stack_map_table	stack_map_table;
	LineNumberTable_attribute LineNumberTable_attribute;
	
	char		*code;
	/* exceptions table in code_attribute tbi  */

}code_attribute;

typedef struct{
	struct	jvm_stack_frame *prev;
	char	*local_variable_table,*pc,*return_addr;
	short	max_stack,max_locals;
	long	*method,offset;
}jvm_stack_frame;
