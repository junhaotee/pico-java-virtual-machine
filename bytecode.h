/* structure and type definitions  */
typedef void (*funcPtr)(short* stackPtr,short* localVarPtr,short* heapPtr);

typedef struct{

	short opcode_size;
	funcPtr interpreter_ptr;
	char* name;

}bytecode_struct;

bytecode_struct bytecode[256];

/* implementation of bytecode */
void bc_nop(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_aconst_null(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_iconst_0(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_iconst_1(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_iconst_2(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_iconst_3(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_iconst_4(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_iconst_5(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_lconst_0(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_lconst_1(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_fconst_0(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_fconst_1(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_fconst_2(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_dconst_0(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_dconst_1(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_bipush(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_sipush(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_ldc(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_ldc_w(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_ldc2_w(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_iload(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_lload(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_fload(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_dload(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_aload(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_iload_0(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_iload_1(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_iload_2(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_iload_3(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_fload_0(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_fload_1(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_fload_2(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_fload_3(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_dload_0(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_dload_1(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_dload_2(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_dload_3(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_aload_0(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_aload_1(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_aload_2(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_aload_3(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_laload(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_faload(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_daload(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_aaload(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_baload(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_caload(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_saload(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_istore(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_lstore(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_fstore(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_dstore(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_astore(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_istore_1(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_istore_2(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_istore_3(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_lstore_0(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_lstore_1(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_lstore_2(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_lstore_3(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_fstore_0(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_fstore_1(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_fstore_2(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_fstore_3(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_dstore_0(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_dstore_1(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_dstore_2(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_dstore_3(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_astore_0(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_astore_1(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_astore_2(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_astore_3(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_lastore(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_fastore(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_dastore(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_aastore(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_bastore(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_castore(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_sastore(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_pop(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_pop2(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_dup(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_dup_x1(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_dup_x2(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_dup2(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_dup2_x1(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_dup2_x2(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_swap(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_iadd(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_ladd(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_fadd(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_dadd(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_isub(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_lsub(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_fsub(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_dsub(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_imul(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_lmul(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_fmul(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_dmul(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_idiv(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_ldiv(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_fdiv(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_ddiv(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_irem(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_lrem(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_frem(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_drem(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_ineg(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_lneg(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_fneg(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_dneg(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_ishl(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_lshl(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_ishr(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_lshr(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_iushr(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_lushr(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_iand(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_land(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_ior(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_lor(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_ixor(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_lxor(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_iinc(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_i2l(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_i2f(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_i2d(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_l2i(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_l2f(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_l2d(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_f2i(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_f2l(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_f2d(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_d2i(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_d2l(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_d2f(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_i2b(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_i2c(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_i2s(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_lcmp(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_fcmpl(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_fcmpg(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_dcmpl(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_dcmpg(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_ifeq(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_ifne(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_iflt(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_ifge(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_ifgt(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_ifle(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_if_icmpeq(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_if_icmpne(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_if_icmplt(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_if_icmpge(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_if_icmpgt(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_if_icmple(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_if_acmpeq(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_if_acmpne(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_goto(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_jsr(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_ret(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_tableswitch(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_lookupswitch(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_ireturn(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_lreturn(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_freturn(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_dreturn(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_areturn(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_return(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_getstatic(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_putstatic(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_getfield(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_putfield(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_invokevirtual(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_invokespecial(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_invokestatic(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_invokeinterface(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_invokedynamic(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_new(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_anewarray(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_arraylength(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_athrow(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_checkcast(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_instanceof(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_monitorenter(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_monitorexit(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_wide(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_multianewarray(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_ifnull(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_ifnonnull(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_goto_w(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_jsr_w(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_breakpoint(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_no_name(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_impdep1(short* stackPtr, short* localVarPtr, short* heapPtr){}
void bc_impdep2(short* stackPtr, short* localVarPtr, short* heapPtr){}

/* pointers declarations */
void (*bc_nop_ptr)(short*,short*,short*)=bc_nop;
void (*bc_aconst_null_ptr)(short*,short*,short*)=bc_aconst_null;
void (*bc_iconst_0_ptr)(short*,short*,short*)=bc_iconst_0;
void (*bc_iconst_1_ptr)(short*,short*,short*)=bc_iconst_1;
void (*bc_iconst_2_ptr)(short*,short*,short*)=bc_iconst_2;
void (*bc_iconst_3_ptr)(short*,short*,short*)=bc_iconst_3;
void (*bc_iconst_4_ptr)(short*,short*,short*)=bc_iconst_4;
void (*bc_iconst_5_ptr)(short*,short*,short*)=bc_iconst_5;
void (*bc_lconst_0_ptr)(short*,short*,short*)=bc_lconst_0;
void (*bc_lconst_1_ptr)(short*,short*,short*)=bc_lconst_1;
void (*bc_fconst_0_ptr)(short*,short*,short*)=bc_fconst_0;
void (*bc_fconst_1_ptr)(short*,short*,short*)=bc_fconst_1;
void (*bc_fconst_2_ptr)(short*,short*,short*)=bc_fconst_2;
void (*bc_dconst_0_ptr)(short*,short*,short*)=bc_dconst_0;
void (*bc_dconst_1_ptr)(short*,short*,short*)=bc_dconst_1;
void (*bc_bipush_ptr)(short*,short*,short*)=bc_bipush;
void (*bc_sipush_ptr)(short*,short*,short*)=bc_sipush;
void (*bc_ldc_ptr)(short*,short*,short*)=bc_ldc;
void (*bc_ldc_w_ptr)(short*,short*,short*)=bc_ldc_w;
void (*bc_ldc2_w_ptr)(short*,short*,short*)=bc_ldc2_w;
void (*bc_iload_ptr)(short*,short*,short*)=bc_iload;
void (*bc_lload_ptr)(short*,short*,short*)=bc_lload;
void (*bc_fload_ptr)(short*,short*,short*)=bc_fload;
void (*bc_dload_ptr)(short*,short*,short*)=bc_dload;
void (*bc_aload_ptr)(short*,short*,short*)=bc_aload;
void (*bc_iload_0_ptr)(short*,short*,short*)=bc_iload_0;
void (*bc_iload_1_ptr)(short*,short*,short*)=bc_iload_1;
void (*bc_iload_2_ptr)(short*,short*,short*)=bc_iload_2;
void (*bc_iload_3_ptr)(short*,short*,short*)=bc_iload_3;
void (*bc_fload_0_ptr)(short*,short*,short*)=bc_fload_0;
void (*bc_fload_1_ptr)(short*,short*,short*)=bc_fload_1;
void (*bc_fload_2_ptr)(short*,short*,short*)=bc_fload_2;
void (*bc_fload_3_ptr)(short*,short*,short*)=bc_fload_3;
void (*bc_dload_0_ptr)(short*,short*,short*)=bc_dload_0;
void (*bc_dload_1_ptr)(short*,short*,short*)=bc_dload_1;
void (*bc_dload_2_ptr)(short*,short*,short*)=bc_dload_2;
void (*bc_dload_3_ptr)(short*,short*,short*)=bc_dload_3;
void (*bc_aload_0_ptr)(short*,short*,short*)=bc_aload_0;
void (*bc_aload_1_ptr)(short*,short*,short*)=bc_aload_1;
void (*bc_aload_2_ptr)(short*,short*,short*)=bc_aload_2;
void (*bc_aload_3_ptr)(short*,short*,short*)=bc_aload_3;
void (*bc_laload_ptr)(short*,short*,short*)=bc_laload;
void (*bc_faload_ptr)(short*,short*,short*)=bc_faload;
void (*bc_daload_ptr)(short*,short*,short*)=bc_daload;
void (*bc_aaload_ptr)(short*,short*,short*)=bc_aaload;
void (*bc_baload_ptr)(short*,short*,short*)=bc_baload;
void (*bc_caload_ptr)(short*,short*,short*)=bc_caload;
void (*bc_saload_ptr)(short*,short*,short*)=bc_saload;
void (*bc_istore_ptr)(short*,short*,short*)=bc_istore;
void (*bc_lstore_ptr)(short*,short*,short*)=bc_lstore;
void (*bc_fstore_ptr)(short*,short*,short*)=bc_fstore;
void (*bc_dstore_ptr)(short*,short*,short*)=bc_dstore;
void (*bc_astore_ptr)(short*,short*,short*)=bc_astore;
void (*bc_istore_1_ptr)(short*,short*,short*)=bc_istore_1;
void (*bc_istore_2_ptr)(short*,short*,short*)=bc_istore_2;
void (*bc_istore_3_ptr)(short*,short*,short*)=bc_istore_3;
void (*bc_lstore_0_ptr)(short*,short*,short*)=bc_lstore_0;
void (*bc_lstore_1_ptr)(short*,short*,short*)=bc_lstore_1;
void (*bc_lstore_2_ptr)(short*,short*,short*)=bc_lstore_2;
void (*bc_lstore_3_ptr)(short*,short*,short*)=bc_lstore_3;
void (*bc_fstore_0_ptr)(short*,short*,short*)=bc_fstore_0;
void (*bc_fstore_1_ptr)(short*,short*,short*)=bc_fstore_1;
void (*bc_fstore_2_ptr)(short*,short*,short*)=bc_fstore_2;
void (*bc_fstore_3_ptr)(short*,short*,short*)=bc_fstore_3;
void (*bc_dstore_0_ptr)(short*,short*,short*)=bc_dstore_0;
void (*bc_dstore_1_ptr)(short*,short*,short*)=bc_dstore_1;
void (*bc_dstore_2_ptr)(short*,short*,short*)=bc_dstore_2;
void (*bc_dstore_3_ptr)(short*,short*,short*)=bc_dstore_3;
void (*bc_astore_0_ptr)(short*,short*,short*)=bc_astore_0;
void (*bc_astore_1_ptr)(short*,short*,short*)=bc_astore_1;
void (*bc_astore_2_ptr)(short*,short*,short*)=bc_astore_2;
void (*bc_astore_3_ptr)(short*,short*,short*)=bc_astore_3;
void (*bc_lastore_ptr)(short*,short*,short*)=bc_lastore;
void (*bc_fastore_ptr)(short*,short*,short*)=bc_fastore;
void (*bc_dastore_ptr)(short*,short*,short*)=bc_dastore;
void (*bc_aastore_ptr)(short*,short*,short*)=bc_aastore;
void (*bc_bastore_ptr)(short*,short*,short*)=bc_bastore;
void (*bc_castore_ptr)(short*,short*,short*)=bc_castore;
void (*bc_sastore_ptr)(short*,short*,short*)=bc_sastore;
void (*bc_pop_ptr)(short*,short*,short*)=bc_pop;
void (*bc_pop2_ptr)(short*,short*,short*)=bc_pop2;
void (*bc_dup_ptr)(short*,short*,short*)=bc_dup;
void (*bc_dup_x1_ptr)(short*,short*,short*)=bc_dup_x1;
void (*bc_dup_x2_ptr)(short*,short*,short*)=bc_dup_x2;
void (*bc_dup2_ptr)(short*,short*,short*)=bc_dup2;
void (*bc_dup2_x1_ptr)(short*,short*,short*)=bc_dup2_x1;
void (*bc_dup2_x2_ptr)(short*,short*,short*)=bc_dup2_x2;
void (*bc_swap_ptr)(short*,short*,short*)=bc_swap;
void (*bc_iadd_ptr)(short*,short*,short*)=bc_iadd;
void (*bc_ladd_ptr)(short*,short*,short*)=bc_ladd;
void (*bc_fadd_ptr)(short*,short*,short*)=bc_fadd;
void (*bc_dadd_ptr)(short*,short*,short*)=bc_dadd;
void (*bc_isub_ptr)(short*,short*,short*)=bc_isub;
void (*bc_lsub_ptr)(short*,short*,short*)=bc_lsub;
void (*bc_fsub_ptr)(short*,short*,short*)=bc_fsub;
void (*bc_dsub_ptr)(short*,short*,short*)=bc_dsub;
void (*bc_imul_ptr)(short*,short*,short*)=bc_imul;
void (*bc_lmul_ptr)(short*,short*,short*)=bc_lmul;
void (*bc_fmul_ptr)(short*,short*,short*)=bc_fmul;
void (*bc_dmul_ptr)(short*,short*,short*)=bc_dmul;
void (*bc_idiv_ptr)(short*,short*,short*)=bc_idiv;
void (*bc_ldiv_ptr)(short*,short*,short*)=bc_ldiv;
void (*bc_fdiv_ptr)(short*,short*,short*)=bc_fdiv;
void (*bc_ddiv_ptr)(short*,short*,short*)=bc_ddiv;
void (*bc_irem_ptr)(short*,short*,short*)=bc_irem;
void (*bc_lrem_ptr)(short*,short*,short*)=bc_lrem;
void (*bc_frem_ptr)(short*,short*,short*)=bc_frem;
void (*bc_drem_ptr)(short*,short*,short*)=bc_drem;
void (*bc_ineg_ptr)(short*,short*,short*)=bc_ineg;
void (*bc_lneg_ptr)(short*,short*,short*)=bc_lneg;
void (*bc_fneg_ptr)(short*,short*,short*)=bc_fneg;
void (*bc_dneg_ptr)(short*,short*,short*)=bc_dneg;
void (*bc_ishl_ptr)(short*,short*,short*)=bc_ishl;
void (*bc_lshl_ptr)(short*,short*,short*)=bc_lshl;
void (*bc_ishr_ptr)(short*,short*,short*)=bc_ishr;
void (*bc_lshr_ptr)(short*,short*,short*)=bc_lshr;
void (*bc_iushr_ptr)(short*,short*,short*)=bc_iushr;
void (*bc_lushr_ptr)(short*,short*,short*)=bc_lushr;
void (*bc_iand_ptr)(short*,short*,short*)=bc_iand;
void (*bc_land_ptr)(short*,short*,short*)=bc_land;
void (*bc_ior_ptr)(short*,short*,short*)=bc_ior;
void (*bc_lor_ptr)(short*,short*,short*)=bc_lor;
void (*bc_ixor_ptr)(short*,short*,short*)=bc_ixor;
void (*bc_lxor_ptr)(short*,short*,short*)=bc_lxor;
void (*bc_iinc_ptr)(short*,short*,short*)=bc_iinc;
void (*bc_i2l_ptr)(short*,short*,short*)=bc_i2l;
void (*bc_i2f_ptr)(short*,short*,short*)=bc_i2f;
void (*bc_i2d_ptr)(short*,short*,short*)=bc_i2d;
void (*bc_l2i_ptr)(short*,short*,short*)=bc_l2i;
void (*bc_l2f_ptr)(short*,short*,short*)=bc_l2f;
void (*bc_l2d_ptr)(short*,short*,short*)=bc_l2d;
void (*bc_f2i_ptr)(short*,short*,short*)=bc_f2i;
void (*bc_f2l_ptr)(short*,short*,short*)=bc_f2l;
void (*bc_f2d_ptr)(short*,short*,short*)=bc_f2d;
void (*bc_d2i_ptr)(short*,short*,short*)=bc_d2i;
void (*bc_d2l_ptr)(short*,short*,short*)=bc_d2l;
void (*bc_d2f_ptr)(short*,short*,short*)=bc_d2f;
void (*bc_i2b_ptr)(short*,short*,short*)=bc_i2b;
void (*bc_i2c_ptr)(short*,short*,short*)=bc_i2c;
void (*bc_i2s_ptr)(short*,short*,short*)=bc_i2s;
void (*bc_lcmp_ptr)(short*,short*,short*)=bc_lcmp;
void (*bc_fcmpl_ptr)(short*,short*,short*)=bc_fcmpl;
void (*bc_fcmpg_ptr)(short*,short*,short*)=bc_fcmpg;
void (*bc_dcmpl_ptr)(short*,short*,short*)=bc_dcmpl;
void (*bc_dcmpg_ptr)(short*,short*,short*)=bc_dcmpg;
void (*bc_ifeq_ptr)(short*,short*,short*)=bc_ifeq;
void (*bc_ifne_ptr)(short*,short*,short*)=bc_ifne;
void (*bc_iflt_ptr)(short*,short*,short*)=bc_iflt;
void (*bc_ifge_ptr)(short*,short*,short*)=bc_ifge;
void (*bc_ifgt_ptr)(short*,short*,short*)=bc_ifgt;
void (*bc_ifle_ptr)(short*,short*,short*)=bc_ifle;
void (*bc_if_icmpeq_ptr)(short*,short*,short*)=bc_if_icmpeq;
void (*bc_if_icmpne_ptr)(short*,short*,short*)=bc_if_icmpne;
void (*bc_if_icmplt_ptr)(short*,short*,short*)=bc_if_icmplt;
void (*bc_if_icmpge_ptr)(short*,short*,short*)=bc_if_icmpge;
void (*bc_if_icmpgt_ptr)(short*,short*,short*)=bc_if_icmpgt;
void (*bc_if_icmple_ptr)(short*,short*,short*)=bc_if_icmple;
void (*bc_if_acmpeq_ptr)(short*,short*,short*)=bc_if_acmpeq;
void (*bc_if_acmpne_ptr)(short*,short*,short*)=bc_if_acmpne;
void (*bc_goto_ptr)(short*,short*,short*)=bc_goto;
void (*bc_jsr_ptr)(short*,short*,short*)=bc_jsr;
void (*bc_ret_ptr)(short*,short*,short*)=bc_ret;
void (*bc_tableswitch_ptr)(short*,short*,short*)=bc_tableswitch;
void (*bc_lookupswitch_ptr)(short*,short*,short*)=bc_lookupswitch;
void (*bc_ireturn_ptr)(short*,short*,short*)=bc_ireturn;
void (*bc_lreturn_ptr)(short*,short*,short*)=bc_lreturn;
void (*bc_freturn_ptr)(short*,short*,short*)=bc_freturn;
void (*bc_dreturn_ptr)(short*,short*,short*)=bc_dreturn;
void (*bc_areturn_ptr)(short*,short*,short*)=bc_areturn;
void (*bc_return_ptr)(short*,short*,short*)=bc_return;
void (*bc_getstatic_ptr)(short*,short*,short*)=bc_getstatic;
void (*bc_putstatic_ptr)(short*,short*,short*)=bc_putstatic;
void (*bc_getfield_ptr)(short*,short*,short*)=bc_getfield;
void (*bc_putfield_ptr)(short*,short*,short*)=bc_putfield;
void (*bc_invokevirtual_ptr)(short*,short*,short*)=bc_invokevirtual;
void (*bc_invokespecial_ptr)(short*,short*,short*)=bc_invokespecial;
void (*bc_invokestatic_ptr)(short*,short*,short*)=bc_invokestatic;
void (*bc_invokeinterface_ptr)(short*,short*,short*)=bc_invokeinterface;
void (*bc_invokedynamic_ptr)(short*,short*,short*)=bc_invokedynamic;
void (*bc_new_ptr)(short*,short*,short*)=bc_new;
void (*bc_anewarray_ptr)(short*,short*,short*)=bc_anewarray;
void (*bc_arraylength_ptr)(short*,short*,short*)=bc_arraylength;
void (*bc_athrow_ptr)(short*,short*,short*)=bc_athrow;
void (*bc_checkcast_ptr)(short*,short*,short*)=bc_checkcast;
void (*bc_instanceof_ptr)(short*,short*,short*)=bc_instanceof;
void (*bc_monitorenter_ptr)(short*,short*,short*)=bc_monitorenter;
void (*bc_monitorexit_ptr)(short*,short*,short*)=bc_monitorexit;
void (*bc_wide_ptr)(short*,short*,short*)=bc_wide;
void (*bc_multianewarray_ptr)(short*,short*,short*)=bc_multianewarray;
void (*bc_ifnull_ptr)(short*,short*,short*)=bc_ifnull;
void (*bc_ifnonnull_ptr)(short*,short*,short*)=bc_ifnonnull;
void (*bc_goto_w_ptr)(short*,short*,short*)=bc_goto;
void (*bc_jsr_w_ptr)(short*,short*,short*)=bc_jsr_w;
void (*bc_breakpoint_ptr)(short*,short*,short*)=bc_breakpoint;
void (*bc_no_name_ptr)(short*,short*,short*)=bc_no_name;
void (*bc_impdep1_ptr)(short*,short*,short*)=bc_impdep1;
void (*bc_impdep2_ptr)(short*,short*,short*)=bc_impdep2;

void bytecodeInit(){
	/* bindings between bytecode array and functions */
}
