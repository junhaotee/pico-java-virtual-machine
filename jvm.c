#include "jlib.h"
#include "bytecode.h"
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>

/* function declarations */
int find_method(char *method_name,method_info method_info[]);
void interpreter();

/* field declarations */
jvm_stack_frame *jsframe,*jsframe_ptr;

constant_info_struct *constant_info;

unsigned short 	minor_version,
		major_version,
		constant_pool_count,
		access_flags,
		this_class,
		super_class,
		interfaces_count,//leave out interfaces[]
		fields_count,
		methods_count,
		attributes_count;

int counter=0;
int inner_counter=0;
int innermost_counter=0;

/* function definitions  */
//big endian to little endian, swap 2 bytes
short b2s(char* ptr,int index){
	return ((0x00FF & ptr[index+1])|
		((ptr[index] << 8) & 0xFF00));
}

int b4s(char* ptr,int index){
	return (((0xFF000000)&(ptr[index]<<24))|
		((0x00FF0000)&(ptr[index+1])<<16)|
		((0x0000FF00)&(ptr[index+2])<<8)|
		((0x000000FF)&(ptr[index+3]))
		);
}

void printFlags(short access_flags){

	if(access_flags & ACC_PUBLIC)
		printf("Public ");
	if(access_flags & ACC_PRIVATE)
		printf("Private ");
	if(access_flags & ACC_PROTECTED)
		printf("Protected ");
	if(access_flags & ACC_VOLATILE)
		printf("Volatile ");
	if(access_flags & ACC_STATIC)
		printf("Static ");
	if(access_flags & ACC_TRANSIENT)
		printf("Transient ");
	if(access_flags & ACC_FINAL)
		printf("Final ");
	if(access_flags & ACC_SUPER)
		printf("Super ");
	if(access_flags & ACC_INTERFACE)
		printf("Interface ");
	if(access_flags & ACC_ABSTRACT)
		printf("Abstract ");
	if(access_flags & ACC_SYNTHETIC)
		printf("Synthetic ");
	if(access_flags & ACC_ANNOTATION)
		printf("Annotation ");
	if(access_flags & ACC_ENUM)
		printf("ENUM ");
	printf("\n");

}


int main(int argc, char** argv){

	struct stat f_stat;
	int class_fd=open(argv[1],O_RDONLY);
	if(class_fd==-1)
		printf("err");
	
	if(stat(argv[1],&f_stat) == -1){
		printf("err stat");
		close(class_fd);
	}
	printf("==========Parsing [%s]==========\n",argv[1]);
	void *clsPtr=mmap(NULL,f_stat.st_size,PROT_READ,MAP_PRIVATE,class_fd,0);
	if(!clsPtr)
		printf("Pointer error");

	printf("Class file mapped at [%p]\n",clsPtr);

	unsigned char *p=(unsigned char *)clsPtr;
	
	printf("MagicNum :[%01x%01x%01x%01x] \n",p[0],p[1],p[2],p[3]); //ca
	printf("Minor Ver:[%u] \nMajor Ver:[%u]\nConstant Pool Count:[%u]\n",minor_version=b2s(p,4),major_version=b2s(p,6),constant_pool_count=b2s(p,8));
	
	//moves pointer to constant pool and iterates through pool data
	printf("==========Parsing Constant Pool==========\n");
	constant_info=malloc(sizeof(constant_info_struct)*constant_pool_count);
	
	p+=10;
	for(counter=1; counter<=constant_pool_count-1; counter++){

		switch(p[0]){
			//not all structures are used in this case, only utf8 is used
			//for unused structures, only print out the information

			case CONSTANT_Class:{
				printf("[%d]CONSTANT_Class\n",counter);
				constant_info[counter].index=counter;
				constant_info[counter].tag=CONSTANT_Class;
				constant_info[counter].name_index=b2s(p,1);
				
				p+=3;
				break;}
			case CONSTANT_Fieldref:{
				printf("[%d]CONSTANT_Fieldref\n",counter);
				constant_info[counter].index=counter;
				constant_info[counter].tag=CONSTANT_Fieldref;
				constant_info[counter].class_index=b2s(p,1);
				constant_info[counter].name_and_type_index=b2s(p,3);
				p+=5;
				break;}
			case CONSTANT_Methodref:{
				printf("[%d]CONSTANT_Methodref\n",counter);
				constant_info[counter].index=counter;
				constant_info[counter].tag=CONSTANT_Methodref;
				constant_info[counter].class_index=b2s(p,1);
				constant_info[counter].name_and_type_index=b2s(p,3);

				p+=5;
				break;}
			case CONSTANT_InterfaceMethodref:{
				printf("[%d]CONSTANT_InterfaceMethodref\n",counter);
				constant_info[counter].index=counter;
				constant_info[counter].tag=CONSTANT_InterfaceMethodref;
				constant_info[counter].class_index=b2s(p,1);
				constant_info[counter].name_and_type_index=b2s(p,3);

				p+=5;
				break;}
			case CONSTANT_String:{
				printf("[%d]CONSTANT_String\n",counter);
				constant_info[counter].index=counter;
				constant_info[counter].tag=CONSTANT_String;
				constant_info[counter].string_index=b2s(p,1);
				p+=3;
				break;}
			case CONSTANT_Integer:{
				printf("[%d]CONSTANT_Integer\n",counter);
				constant_info[counter].index=counter;
				constant_info[counter].tag=CONSTANT_Integer;
				constant_info[counter].bytes=b4s(p,1);
				p+=5;
				break;}
			case CONSTANT_Float:{
				printf("[%d]CONSTANT_Float\n",counter);
				constant_info[counter].index=counter;
				constant_info[counter].tag=CONSTANT_Float;
				constant_info[counter].bytes=b4s(p,1);

				p+=5;
				break;}
			case CONSTANT_Long:{
				printf("[%d]CONSTANT_Long\n",counter);
				constant_info[counter].index=counter;
				constant_info[counter].tag=CONSTANT_Long;
				constant_info[counter].high_bytes=b4s(p,1);
				constant_info[counter].low_bytes=b4s(p,5);
				p+=9;
				break;}
			case CONSTANT_Double:{
				printf("[%d]CONSTANT_Double\n",counter);
				constant_info[counter].index=counter;
				constant_info[counter].tag=CONSTANT_Double;
				constant_info[counter].high_bytes=b4s(p,1);
				constant_info[counter].low_bytes=b4s(p,5);

				p+=9;
				break;}
			case CONSTANT_NameAndType:{
				printf("[%d]CONSTANT_NameAndType\n",counter);
				constant_info[counter].index=counter;
				constant_info[counter].tag=CONSTANT_NameAndType;
				constant_info[counter].name_index=b2s(p,1);
				constant_info[counter].descriptor_index=b2s(p,3);
				p+=5;
				break;}
			case CONSTANT_Utf8:{
				printf("[%d]CONSTANT_Utf8\n",counter);
				short length=b2s(p,1);
				p+=3;
				
				char	*buf;
				buf=malloc(length+1);
				buf[length]='\0';
				memcpy(buf,p,length);
		
				constant_info[counter].index=counter;
				constant_info[counter].base=buf;
				constant_info[counter].tag=CONSTANT_Utf8;

				p+=length;

				break;}
			case CONSTANT_MethodHandle:{
				printf("[%d]CONSTANT_MethodHandle\n",counter);
				constant_info[counter].index=counter;
				constant_info[counter].tag=CONSTANT_MethodHandle;
				constant_info[counter].reference_kind=p[1];
				constant_info[counter].reference_index=b2s(p,2);
				p+=4;
				break;}
			case CONSTANT_MethodType:{
				printf("[%d]CONSTANT_MethodType\n",counter);
				constant_info[counter].index=counter;
				constant_info[counter].tag=CONSTANT_MethodType;
				constant_info[counter].descriptor_index=b2s(p,1);
				p+=3;
				break;}
			case CONSTANT_InvokeDynamic:{
				printf("[%d]CONSTANT_InvokeDynamic\n",counter);
				constant_info[counter].index=counter;
				constant_info[counter].tag=CONSTANT_InvokeDynamic;
				constant_info[counter].bootstrap_method_attr_index=b2s(p,1);
				constant_info[counter].name_and_type_index=b2s(p,3);
				p+=5;
				break;}
		}
	}

	printf("==========Print Constant Pool==========\n");

	for(counter=0;counter<=constant_pool_count-1;counter++){

		switch(constant_info[counter].tag){

		case CONSTANT_Class:{
			printf("[%d]%s\n",
				counter,
				constant_info[constant_info[counter].name_index].base);
			break;
		}
		case CONSTANT_Fieldref:{
		}
		case CONSTANT_Methodref:{
		}
		case CONSTANT_InterfaceMethodref:{
			printf("[%d]%s(%s)\n",
			counter,
			constant_info[constant_info[constant_info[counter].class_index].name_index].base,
			constant_info[constant_info[constant_info[counter].name_and_type_index].name_index].base
			);
			break;
		}
		case CONSTANT_String:{
			printf("[%d]%s\n",
				counter,
				constant_info[constant_info[counter].string_index].base);
			break;
		}
		case CONSTANT_Integer:{
			printf("[%d]%d",counter,constant_info[counter].bytes);
			break;
		}
		case CONSTANT_Float:{
			printf("[%d]%f",counter,constant_info[counter].bytes);
			break;
		}
		case CONSTANT_Long:{
			//tbi
			break;
		}
		case CONSTANT_Double:{
			//tbi
			break;
		}
		case CONSTANT_NameAndType:{
			printf("[%d]%s(%s)\n",
				counter,
				constant_info[constant_info[counter].name_index].base,
				constant_info[constant_info[counter].descriptor_index].base);
			break;
		}
		case CONSTANT_Utf8:{
			printf("[%d]%s\n",counter,constant_info[counter].base);
			break;
		}
		case CONSTANT_MethodHandle:{
			//tbi
			break;
		}
		case CONSTANT_MethodType:{
			printf("[%d]%s\n",
			counter,
			constant_info[constant_info[counter].descriptor_index].base);
			break;
		}
		case CONSTANT_InvokeDynamic:{
			//tbi
			break;
		}

		}

		//if(constant_info[counter].base!=NULL)
		//printf("[%d]:%s\n",constant_info[counter].index,constant_info[counter].base);

	}

	//obtain marked access flag
	printf("==========Parsing Access Flags==========\n");
	access_flags=b2s(p,0);
	printf("access_flags:%x\n",access_flags);
	char flag=0;

	if(access_flags & ACC_PUBLIC)
		flag|=0b00000001;
	if(access_flags & ACC_FINAL)
		flag|=0b00000010;
	if(access_flags & ACC_SUPER)
		flag|=0b00000100;
	if(access_flags & ACC_INTERFACE)
		flag|=0b00001000;
	if(access_flags & ACC_ABSTRACT)
		flag|=0b00010000;
	if(access_flags & ACC_SYNTHETIC)
		flag|=0b00100000;
	if(access_flags & ACC_ANNOTATION)
		flag|=0b01000000;
	if(access_flags & ACC_ENUM)
		flag|=0b10000000;


	//get this_class
	p+=2;
	printf("this_class:%d\n",this_class=b2s(p,0));

	//get super_class;
	p+=2;
	printf("super_class:%d\n",super_class=b2s(p,0));

	//get interfaces count
	p+=2;
	printf("interfaces_count:%d\n",interfaces_count=b2s(p,0));

	//get interface data if exists
	if(interfaces_count>0){
	}

	//get fields_count
	p+=2;
	printf("fields_count:%d\n",fields_count=b2s(p,0));
	p+=2; //increment to access flags


	//iterates through all fields info
	printf("==========Parsing Field Info==========\n");
	if(fields_count>0){
		
		field_info field_info[fields_count];

		for(counter=0; counter<fields_count; counter++){

			//get access_flags
			field_info[counter].access_flags=b2s(p,0);
			
			//get name_index
			field_info[counter].name_index=b2s(p,2);

			//get descriptor_index;
			field_info[counter].descriptor_index=b2s(p,4);

			//get attributes count 
			field_info[counter].attributes_count=b2s(p,8);
			
			p+=8; //increment from attributes count
			

			if(!strcmp(constant_info[b2s(p,0)].base,"ConstantValue")){

		
				field_info[counter].ConstantValue_attribute.attribute_name_index=b2s(p,0);
				p+=2;

				field_info[counter].ConstantValue_attribute.attribute_length=b4s(p,0);
				p+=4;

				field_info[counter].ConstantValue_attribute.constantvalue_index=b2s(p,0);
				p+=2;

				printf("[%d]",counter);
				printFlags(field_info[counter].access_flags);
				printf("%s\n%s=",
					constant_info[field_info[counter].descriptor_index].base,
					constant_info[field_info[counter].name_index].base
					);
				
				switch(constant_info[field_info[counter].ConstantValue_attribute.constantvalue_index].tag){
					case CONSTANT_Long:{
						//tbi
						break;
					}
					case CONSTANT_Float:{
						printf("%f\\nn",
							constant_info[field_info[counter].ConstantValue_attribute.constantvalue_index].bytes);
						break;
					}
					case CONSTANT_Double:{
						//tbi
						break;
					}
					case CONSTANT_Integer:{
						printf("%d\n\n",
							constant_info[field_info[counter].ConstantValue_attribute.constantvalue_index].bytes);
						break;
					}
					case CONSTANT_String:{
						printf("%s\n\n",
						constant_info[constant_info[field_info[counter].ConstantValue_attribute.constantvalue_index].string_index].base);
						
						break;
					}
				}


			}else if(!strcmp(constant_info[b2s(p,0)].base,"Synthetic")){
				printf("Synthetic\n");
				p+=8;
				//to be implemented
			}else if(!strcmp(constant_info[b2s(p,0)].base,"Signature")){
				printf("Singature\n");
				p+=8;
				//to be implemented
			}else if(!strcmp(constant_info[b2s(p,0)].base,"Deprecated")){
				printf("Deprecated\n");
				p+=8;
				//to be implemented
			}else if(!strcmp(constant_info[b2s(p,0)].base,"RuntimeVisibleAnnotations")){
				printf("RuntimeVisibleAnnotations\n");
				p+=8;
				//to be implemented
			}else if(!strcmp(constant_info[b2s(p,0)].base,"RuntimeInvisibleAnnotations")){
				//to be implemented
				printf("RuntimeInvisibleAnnotations\n");p+=8;
			}else{
				printf("uncaught\n");p+=8;
			}

			
		}
	}
	
	//get methods count
	printf("==========Parsing Method Info==========\n");
	methods_count=b2s(p,0);
	printf("methods count:%d\n",methods_count);
	p+=2;

	method_info method_info[methods_count];
	code_attribute	code_attr[methods_count];
	char *code_ptr[methods_count];
	if(methods_count>0){
	
	
		for(counter=0;counter<methods_count;counter++){
			
			method_info[counter].access_flags=b2s(p,0);
			p+=2;

			method_info[counter].name_index=b2s(p,0);
			p+=2;

			method_info[counter].descriptor_index=b2s(p,0);
			p+=2;

			method_info[counter].attributes_count=b2s(p,0);
			p+=2;

			printf("[%d]%s\n",counter,constant_info[method_info[counter].name_index].base);
			printFlags(method_info[counter].access_flags);
			printf("%s\n",constant_info[method_info[counter].descriptor_index].base);
			
			printf("%s:\n",constant_info[b2s(p,0)].base);

			if(strcmp("Code",constant_info[b2s(p,0)].base)==0){
				
				code_attr[counter].attribute_name_index=b2s(p,0);
				p+=2;

				code_attr[counter].attribute_length=b4s(p,0);
				p+=4;

				code_attr[counter].max_stack=b2s(p,0);
				p+=2;
				

				code_attr[counter].max_locals=b2s(p,0);
				p+=2;
				printf("max_stacks:%d max_locals:%d \n",
						code_attr[counter].max_stack,
						code_attr[counter].max_locals);

				code_attr[counter].code_length=b4s(p,0);
				p+=4;
				
				//copy all the bytecode into code structure
				int i=0;
				for(i=0;i<code_attr[counter].code_length;i++){

					printf("bytecode:%x\n",(int)p[i]);
				}
				code_ptr[counter]=(char*)malloc(code_attr[counter].code_length+1);
				printf("malloced address :%x\n",code_ptr[counter]);

				memcpy(code_ptr[counter],p,code_attr[counter].code_length);
				p+=code_attr[counter].code_length;

				printf("first byte:%x\n",*code_ptr[counter]);


				//tbi::exception tables
				//program aborts if exception table length > 0
				assert(b2s(p,0)==0);
				p+=2;

				code_attr[counter].attributes_count=b2s(p,0);
				printf("attributes count in code:%d\n",b2s(p,0));
				p+=2;


				int attribute_counter=0;
				for(attribute_counter=0;
				attribute_counter<code_attr[counter].attributes_count;
				attribute_counter++){

					if(strcmp("LineNumberTable",constant_info[b2s(p,0)].base)==0){
						
						code_attr[counter].LineNumberTable_attribute.attribute_name_index=b2s(p,0);
						printf("index:%d\n",b2s(p,0));
						p+=2;

						code_attr[counter].LineNumberTable_attribute.attribute_length=b4s(p,0);
						printf("attribute_length:%d\n",b4s(p,0));
						p+=4;

						code_attr[counter].LineNumberTable_attribute.line_number_table_length=b2s(p,0);
						printf("line number table length:%d\n",b2s(p,0));
						p+=2;
						
						code_attr[counter].LineNumberTable_attribute.line_number_table[b4s(p,0)];

						int line_number_table_counter=0;
						for(line_number_table_counter=0;
						line_number_table_counter<code_attr[counter].LineNumberTable_attribute.line_number_table_length;
						line_number_table_counter++){
							code_attr[counter].LineNumberTable_attribute.line_number_table[line_number_table_counter].start_pc=b2s(p,0);
							p+=2;
							code_attr[counter].LineNumberTable_attribute.line_number_table[line_number_table_counter].line_number=b2s(p,0);
							p+=2;
						}
		
						printf("\n");

					//tbi
					}else if(strcmp("StackMapTable",constant_info[b2s(p,0)].base)==0){
					}else if(strcmp("LocalVariableTable",constant_info[b2s(p,0)].base)==0){
					}else if(strcmp("LocalVariableTypeTable",constant_info[b2s(p,0)].base)==0){
					}else{
						printf("Uncaught\n");
					}

				}
		
			//tbi
			}else if(strcmp("Exceptions",constant_info[b2s(p,0)].base)==0){
			}else if(strcmp("Signature",constant_info[b2s(p,0)].base)==0){
			}else if(strcmp("Synthetic",constant_info[b2s(p,0)].base)==0){
			}else if(strcmp("Deprecated",constant_info[b2s(p,0)].base)==0){
			}else if(strcmp("RuntimeVisibleAnnotations",constant_info[b2s(p,0)].base)==0){
			}else if(strcmp("RuntimeInvisibleAnnotations",constant_info[b2s(p,0)].base)==0){
			}else if(strcmp("RuntimeVisibleParameterAnnotations",constant_info[b2s(p,0)].base)==0){
			}else if(strcmp("RuntimeVisibleParameterAnnotations",constant_info[b2s(p,0)].base)==0){
			}else if(strcmp("AnnotationDefault",constant_info[b2s(p,0)].base)==0){
			}else{
				printf("Uncaught\n");
			}
			
		}
	}
	attributes_count=b2s(p,0);
	p+=2;

	for(counter=0;counter<attributes_count;counter++){

		if(strcmp("SourceFile",constant_info[b2s(p,0)].base)==0){
			p+=6;
			printf("Sourde File:%s\n",constant_info[b2s(p,0)].base);
		//tbi
		}else if(strcmp("InnerClasses",constant_info[b2s(p,0)].base)==0){
			printf("uncaught");
			assert(0>1);
		}else if(strcmp("EnclosingMethod",constant_info[b2s(p,0)].base)==0){
			printf("enclosing method");
			assert(0>1);
		}else if(strcmp("Synthetic",constant_info[b2s(p,0)].base)==0){
			printf("synthetic");
			assert(0>1);
		}else if(strcmp("Signature",constant_info[b2s(p,0)].base)==0){
			printf("signature");
			assert(0>1);
		}else if(strcmp("SourceDebugExtension",constant_info[b2s(p,0)].base)==0){
			printf("sourcedebugextension");
			assert(0>1);
		}else if(strcmp("Deprecated",constant_info[b2s(p,0)].base)==0){
			printf("deprecated");
			assert(0>1);
		}else if(strcmp("RuntimeVisibleAnnotations",constant_info[b2s(p,0)].base)==0){
			printf("runtimevisibleannotations");
			assert(0>1);
		}else if(strcmp("RuntimeInvisibleAnnotations",constant_info[b2s(p,0)].base)==0){
			printf("runtimeinvisibleannotation");
			assert(0>1);
		}else if(strcmp("BoostrapMethods",constant_info[b2s(p,0)].base)==0){
			printf("bootstrap methods");
			assert(0>1);
		}else{
			printf("uncaught");
			assert(0>1);
		}
	}
	
	printf("addr of method code %x\n",code_ptr[0]);
	printf("addr of method code %x\n",code_ptr[1]);
	printf("addr of method code %x\n",code_ptr[2]);
	printf("addr of method code %x\n",code_ptr[3]);
	printf("addr of method code %x\n",code_ptr[4]);

	printf("first bytecode of main:%x\n",*code_ptr[1]);
	//initializing environment
	jsframe_ptr=NULL;
		

	//create_newFrame(find_method("main",method_info),code_attr);


}

int find_method(char *method_name,method_info method_info[]){

	int counter=0;
	for(counter=0;counter<methods_count;counter++){
		if(strcmp("main",constant_info[method_info[counter].name_index].base)==0){
			printf("main is %d\n",counter);
			break;
		}
	}
	return counter;
}

int create_newFrame(int counter,code_attribute code_attr[]){

	jsframe=(jvm_stack_frame*)malloc(sizeof(jvm_stack_frame));

	jsframe->max_locals=code_attr[counter].max_locals;
	jsframe->max_stack=code_attr[counter].max_stack;
	jsframe->pc=code_attr[counter].code;
	jsframe->offset=code_attr[counter].code_length;
	printf("bc:%x\n",*code_attr[counter].code);
/*	
	int i=0;
	for(i=0;i<jsframe->offset;i++){
		printf("bytecode:%x\n",jsframe->pc);
		jsframe->pc++;
	}
*/
	//if there happen to be a branching
	//jsframe->prev=jsframe_ptr;
	//jsframe_ptr=jsframe;

}
